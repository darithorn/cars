#include <stdlib.h>
#include <math.h>
#include <SDL2/SDL.h>
#include "vector2.h"

#define WHEEL_BASE 40
#define WHEEL_BASE_HALF (WHEEL_BASE / 2)

typedef struct {
    struct motor_s {
        double speed; // aka "rotational speed"
        vector2 pos; // the translated position
    } left_motor, right_motor;
    vector2 right;
    vector2 heading;
    vector2 center;
    vector2 icc;
} car_t;

car_t car_new(float offset_x, float offset_y) {
    struct motor_s left_motor = (struct motor_s) 
                                                { 
                                                    0, 
                                                    vector2_new(offset_x - WHEEL_BASE_HALF, offset_y),
                                                };
    struct motor_s right_motor = (struct motor_s) 
                                                {   0, 
                                                    vector2_new(offset_x + WHEEL_BASE_HALF, offset_y),
                                                };
    car_t self = (car_t) { left_motor, right_motor, vector2_right(), vector2_up(), vector2_new(offset_x, offset_y), vector2_zero() };
    return self;
}

void car_simulate(car_t *self, double dt) {
    // W = (Vr - Vl) / L
    // R = L / 2 * ((Vl - Vr) / (Vr + Vl))
    if(self->right_motor.speed != self->left_motor.speed) {
        float w = (self->right_motor.speed - self->left_motor.speed) / WHEEL_BASE;
        self->right = vector2_rotate(self->right, w * dt);
        self->heading = vector2_rotate(self->heading, w * dt);

        float r = WHEEL_BASE_HALF * ((self->left_motor.speed + self->right_motor.speed) / (self->right_motor.speed - self->left_motor.speed));
        self->icc = vector2_add(vector2_mul(self->right, r), self->center);
        self->left_motor.pos = vector2_rotate_around(self->left_motor.pos, self->icc, w * dt);
        self->right_motor.pos = vector2_rotate_around(self->right_motor.pos, self->icc, w * dt);
    } else {
        self->left_motor.pos = vector2_add(vector2_mul(self->heading, self->left_motor.speed * dt), self->left_motor.pos);
        self->right_motor.pos = vector2_add(vector2_mul(self->heading, self->right_motor.speed * dt), self->right_motor.pos);
    }
    self->center = vector2_midpoint(self->left_motor.pos, self->right_motor.pos);
}

void car_draw(car_t *self, SDL_Renderer *renderer) {
    SDL_Rect left = (SDL_Rect) { self->left_motor.pos.x, self->left_motor.pos.y, 5, 5 };
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);

    vector2 r = vector2_add(vector2_mul(self->right, 10), self->center);
    SDL_RenderDrawLine(renderer, self->center.x, self->center.y, r.x, r.y);
    SDL_RenderDrawRect(renderer, &left);

    SDL_Rect right = (SDL_Rect) { self->right_motor.pos.x, self->right_motor.pos.y, 5, 5 };
    SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);

    vector2 heading = vector2_add(vector2_mul(self->heading, 10), self->center);
    SDL_RenderDrawLine(renderer, self->center.x, self->center.y, heading.x, heading.y);
    SDL_RenderDrawRect(renderer, &right);

    SDL_Rect center = (SDL_Rect) { self->center.x, self->center.y, 5, 5 };
    SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
    // SDL_RenderDrawRect(renderer, &center);

    SDL_Rect icc = (SDL_Rect) { self->icc.x, self->icc.y, 2, 2 };
    SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255);
    SDL_RenderDrawRect(renderer, &icc);
}

int main() {

    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window *window = SDL_CreateWindow(  "cars",     
                                            0,
                                            0, 
                                            640, 480, SDL_WINDOW_SHOWN);

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    car_t car = car_new(500, 240);
    car.left_motor.speed = 10;
    car.right_motor.speed = 50;

    double t = 0.0;
    const double dt = 0.01;
    double cur_time = SDL_GetTicks() / 1000.0;
    double accumulator = 0.0;

    int running = 1;
    SDL_Event event;
    while(running) {

        while(SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_QUIT:
                    running = 0;
                    break;
            }
        }

        double new_time = SDL_GetTicks() / 1000.0;
        double frame_time = new_time - cur_time;
        if(frame_time >= 0.25) {
            frame_time = 0.25;
        }
        cur_time = new_time;

        accumulator += frame_time;

        while(accumulator >= dt) {

            car_simulate(&car, dt);

            accumulator -= dt;
            t += dt;
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        car_draw(&car, renderer);

        SDL_RenderPresent(renderer);
    }
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}