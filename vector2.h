typedef struct {
    float x, y;
} vector2;

#define vector2_debug(self) { printf("x: %f\ty: %f\n", self.x, self.y); }

vector2 vector2_zero() {
    return (vector2) { 0, 0 };
}

vector2 vector2_up() {
    return (vector2) { 0, -1 };
}

vector2 vector2_right() {
    return (vector2) { 1, 0 };
}


vector2 vector2_new(float x, float y) {
    return (vector2) { x, y };
}

float vector2_magnitude(vector2 self) {
    return sqrt((self.x * self.x) + (self.y + self.y));
}

vector2 vector2_normalize(vector2 self) {
    float magnitude = vector2_magnitude(self);
    return (vector2) { self.x / magnitude, self.y / magnitude };
}

vector2 vector2_sub(vector2 a, vector2 b) {
    return (vector2) { a.x - b.x, a.y - b.y };
}

vector2 vector2_add(vector2 a, vector2 b) {
    return (vector2) { a.x + b.x, a.y + b.y };
}

vector2 vector2_mul(vector2 self, float a) {
    return (vector2) { self.x * a, self.y * a };
}

vector2 vector2_rotate(vector2 self, float theta) {
    return (vector2) { 
        self.x * cos(theta) - self.y * sin(theta), 
        self.x * sin(theta) + self.y * cos(theta) 
    };
}

vector2 vector2_rotate_around(vector2 self, vector2 origin, float theta) {
    self.x -= origin.x;
    self.y -= origin.y;
    vector2 pos = vector2_rotate(self, theta);
    pos.x += origin.x;
    pos.y += origin.y;
    return pos;
}

vector2 vector2_midpoint(vector2 a, vector2 b) {
    return (vector2) {
        (a.x + b.x) / 2,
        (a.y + b.y) / 2
    };
}